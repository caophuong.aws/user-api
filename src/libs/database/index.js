const Knex = require('knex');
const connection = require(`${__base}database/config`);
const { Model } = require('objection');

const knexConnection = Knex(connection);

Model.knex(knexConnection);


class User extends Model {
  static get tableName() {
    return 'User';
  }
}

class GlobalConfig extends Model {
  static get tableName() {
    return 'GlobalConfig';
  }
}

export default class Database {
  model() {
    return {
      User,
      GlobalConfig,
    };
  }
}
