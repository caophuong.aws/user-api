import express from 'express';

module.exports = ({accessControl, userHandlers, intercept}) => {
  const router = express.Router();

  router.get('/', intercept(userHandlers, 'get'));

  router.get('/:id([0-9]+)', intercept(userHandlers, 'getById'));

  router.post('/', accessControl.checkToken(), intercept(userHandlers, 'create'));

  router.post('/:id([0-9]+)', accessControl.checkToken(), intercept(userHandlers, 'update'));

  router.delete('/:id([0-9]+)', accessControl.checkToken(), intercept(userHandlers, 'delete'));

  return router;
};
