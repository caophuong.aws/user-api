import express from 'express';

module.exports = ({authHandlers, intercept}) => {
  const router = express.Router();

  router.post('/login', intercept(authHandlers, 'login'));

  return router;
};
