export default class UserModel {
  constructor({ Database }) { 
    this.model = Database.model().User;
  }

  async get(params) {
    return await this.model
      .query()
      .where({
        ...params.filter
      })
      .orderBy(params.order)
      .page(params.page, params.pageSize);
  }

  async getById(id) {
    return await this.model.query()
      .findById(id)
  }

  async getUserByEmail(email) {
    return await this.model.query().findOne({ email })
  }

  async insert(params) {
    return await this.model.query().insert(params);
  }

  async update(id, params) {
    return await this.model.query().updateAndFetchById(id, params);
  }

  async delete(id) {
    return await this.model.query()
      .delete()
      .where({ id });
  }
}
