export default class UserHandler {
  constructor({ userService }) {
    this.userService = userService;
  }

  async get(req, res, next) {
    const { filter } = req;

    const users = await this.userService.get(filter);

    if (!users) {
      return next(ErrorCode.USER_NOT_FOUND);
    }

    users.results = users.results.map((user) => _wrapModel(user));

    return res.pagination(users, filter);
  }

  async getById(req, res, next) {
    const { id } = req.params;

    if (!id) {
      return next(ErrorCode.INVALID_PARAMETER);
    }

    const userData = await this.userService.getById(id);

    if (!userData) {
      return next(ErrorCode.USER_NOT_FOUND);
    }

    return res.send(_wrapModel(userData));
  }

  async getByEmail(req, res, next) {
    const { email } = req.params;

    if (!email) {
      return next(ErrorCode.INVALID_PARAMETER);
    }

    const userData = await this.userService.getByEmail(email);

    if (!userData) {
      return next(ErrorCode.USER_NOT_FOUND);
    }

    return res.send(_wrapModel(userData));
  }

  async create(req, res, next) {
    const params = req.body;
    if (!req.decoded) {
      return next(ErrorCode.PERMISSION_DENIED);
    }
    const userData = await this.userService.create(params, req.decoded);
    return res.send(_wrapModel(userData));
  }

  async update(req, res, next) {
    const params = req.body;
    const {id} = req.params;
    
    if (!id) {
      return next(ErrorCode.INVALID_PARAMETER);
    }

    if (!req.decoded) {
      return next(ErrorCode.PERMISSION_DENIED);
    }
    const userData = await this.userService.update(id, params, req.decoded);
    
    return res.send(_wrapModel(userData));
  }

  async delete(req, res, next) {
    const { id } = req.params;

    if (!id) {
      return next(ErrorCode.INVALID_PARAMETER);
    }

    if (!req.decoded) {
      return next(ErrorCode.PERMISSION_DENIED);
    }

    const deleted = await this.userService.delete(id);

    if (!deleted) {
      return next(ErrorCode.DATA_NOT_FOUND);
    }

    return res.success();
  }
}
