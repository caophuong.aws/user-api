export default class AuthHandler {
    constructor({ authService }) {
        this.authService = authService;
    }

    async login(req, res) {
        const { email, password } = req.body;

        const token = await this.authService.login({ email, password });

        return res.send(token);
    }

}