const jwt = require('jsonwebtoken');
const config = require(`${__config}`);

export default class AuthService {
    constructor({ userModel }) {
        this.userModel = userModel;
    }

    async login(params) {
        const { email, password } = params;

        if (!email | !password) {
            throw ErrorCode.INVALID_PARAMETER;
        }

        const userData = await this.userModel.getUserByEmail(email);
        if (!userData) {
            return {
                success: false,
                message: ErrorCode.USER_NOT_FOUND
            }
        }
        // TO DO: hash password
        if (userData.password === password) {
            const token = jwt.sign(
                {
                    id: userData.id,
                    email: userData.email
                },
                config.tokenSecretKey,
                {
                    expiresIn: '72h'
                }
            );
            
            return {
                success: true,
                message: 'Authentication successful!',
                user: userData.lastName + userData.firstName,
                token: token
            }
        } else {
            return {
                success: false,
                message: ErrorCode.WRONG_PASSWORD
            }
        }
    }
}