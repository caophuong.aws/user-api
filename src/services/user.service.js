import _ from 'lodash';
import moment from 'moment';

export default class UserService {
  constructor({userModel}) {
    this.userModel = userModel;
  }

  async get(params) {
    return await this.userModel.get(params);
  }

  async getById(id) {
    return await this.userModel.getById(id);
  }

  async create(data, user) {
    const currentUser = _.get(user, 'email') || '-';

    const params = {
      ...data,
      createdBy: currentUser
    };

    const userData = await this.userModel.insert(params);

    return userData;
  }

  async update(id, data, user) {
    const currentUser = _.get(user, 'email') || '-';

    const userData = await this.userModel.getById(id);

    if (!userData) {
      throw ErrorCode.USER_NOT_FOUND;
    }

    const params = {
      ...data,
      updatedBy: currentUser,
      updatedAt: moment(),
    };

    await this.userModel.update(id, params);

    return await this.userModel.getById(id);
  }

  async delete(id) {
    return await this.userModel.delete(id);
  }
}
