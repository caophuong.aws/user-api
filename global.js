global.__base = `${__dirname}/`;
global.__apis = `${__dirname}/src/`;
global.__config = `${__apis}config/`;
global.__libs = `${__apis}libs/`;
global.__models = `${__apis}models/`;
global.__services = `${__apis}services/`;
global.__controllers = `${__apis}controllers/`;
global.__routers = `${__apis}routers/`;
global.__utils = `${__apis}utils/`;
global.Constants = require(`${__dirname}/data/Constants`);
global.ErrorCode = require(`${__dirname}/data/ErrorCode`);

global._wrapModel = (user) => {
    const userObj = user.toJSON();
    return userObj;
  };
