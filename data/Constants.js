module.exports = {
  GLOBAL_CONFIG: {
    DATA_TYPE: {
      BOOLEAN: 'BOOLEAN',
      ARRAY: 'ARRAY',
      STRING: 'STRING',
      NUMBER: 'NUMBER',
    },
    VALUE: {
      BOOLEAN: {
        TRUE: 'TRUE',
        FALSE: 'FALSE',
      }
    }
  }
};
