module.exports = {
  PERMISSION_DENIED: {
    code: 4001,
    message: 'PERMISSION_DENIED',
    type: 'BAD_REQUEST'
  },
  INVALID_PARAMETER: {
    code: 3000,
    message: 'INVALID_PARAMETER',
    type: 'BAD_REQUEST'
  },
  INVALID_CODE: {
    code: 3001,
    message: 'INVALID_CODE',
    type: 'BAD_REQUEST'
  },
  INVALID_GLOBAL_CONFIG_TYPE: {
    code: 3002,
    message: 'INVALID_GLOBAL_CONFIG_TYPE',
    type: 'BAD_REQUEST'
  },
  USER_CONFIG_INVALID_INPUT: {
    code: 3002,
    message: 'USER_CONFIG_INVALID_INPUT',
    type: 'BAD_REQUEST'
  },
  USER_EXISTED: {
    code: 3010,
    message: 'USER_EXISTED',
    type: 'BAD_REQUEST'
  },
  WRONG_PASSWORD: {
    code: 3020,
    message: 'WRONG_PASSWORD',
    type: 'BAD_REQUEST'
  },
  USER_VERIFIED: {
    code: 3021,
    message: 'USER_VERIFIED',
    type: 'BAD_REQUEST'
  },
  USER_VERIFICATION_FAILED: {
    code: 3022,
    message: 'USER_VERIFICATION_FAILED',
    type: 'BAD_REQUEST'
  },
  DATA_NOT_FOUND: {
    code: 3100,
    message: 'DATA_NOT_FOUND',
    type: 'NOT_FOUND'
  },
  USER_NOT_FOUND: {
    code: 3101,
    message: 'USER_NOT_FOUND',
    type: 'NOT_FOUND'
  },
  ROLE_NOT_FOUND: {
    code: 3102,
    message: 'ROLE_NOT_FOUND',
    type: 'NOT_FOUND'
  },
  CONFIG_NOT_FOUND: {
    code: 3103,
    message: 'CONFIG_NOT_FOUND',
    type: 'NOT_FOUND'
  },
};
