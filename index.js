require('./global');
require('@babel/register');
require('babel-polyfill');
const express = require('express');
const fs = require('fs');
const cookieParser = require('cookie-parser');


const app = express();
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: false }));
app.use(cookieParser());

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,HEAD,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  next();
});

// default url
app.get('/', (req, res) => { res.send('Welcome demo api')})

// check config api
const config = require(`${__config}`);
app.get('/check-config', (req, res) => res.send(config));
 
// ------------------------------------------------------------ //
const containerMiddleware = require('./src/middlewares/container');
app.use(require('./src/middlewares/response'));
fs.readdirSync(__routers).forEach((route) => {
  if (!route || route[0] === '.') {
    return false;
  }
  return app.use(`/api/${route.split('.')[0]}`, containerMiddleware(route));
});

app.listen(8100, () => {
  console.log('Server is started');
}); 