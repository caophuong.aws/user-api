CREATE TABLE "User" (
  "id" SERIAL UNIQUE PRIMARY KEY,
  "username" varchar UNIQUE,
  "password" varchar NOT NULL,
  "email" varchar NOT NULL UNIQUE,
  "firstName" varchar,
  "lastName" varchar,
  "phoneNumber" varchar,
  "dob" date,
  "desc" varchar,
  "profileImage" varchar,
  "createdBy" varchar,
  "createdAt" TIMESTAMP DEFAULT (now()),
  "updatedBy" varchar,
  "updatedAt" TIMESTAMP DEFAULT (now())
);
CREATE INDEX ON "User" ("email");

INSERT INTO "User"("id", "username", "password", "email", "firstName", "lastName", "phoneNumber","dob", "desc") VALUES (1, 'superadmin', '1234', 'superadmin@fpt.com', 'Admin', 'Super', '967678678', '1994-02-09', NULL);
